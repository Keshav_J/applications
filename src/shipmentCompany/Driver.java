package shipmentCompany;

import java.util.TimeZone;

import java.time.LocalDateTime;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Driver {
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(JUnitTester.class);
		
		for(Failure f: result.getFailures()) {
			System.out.println(f.toString());
		}
		
		System.out.println("Result = " + result.wasSuccessful());
	}
}

class Cargo {
	public LocalDateTime getArrivalDate(LocalDateTime ldt, int hours,
									TimeZone depart_timezone, TimeZone arrive_timezone) {
		int minutes = hours * 60;
		
		while(minutes > 0) {
			int minutesLeftToday = 24 * 60 - (ldt.getHour() * 60 + ldt.getMinute());
			
			if(Constraints.isHoliday(ldt) || Constraints.isGovtHoliday(ldt)) {
				// System.out.println(ldt + " " + this.isHoliday(ldt) + " " + this.isGovtHoliday(ldt) + '\n');
				
				ldt = ldt.plusMinutes(minutesLeftToday);
				continue;
			}
			
			int shippingMinutes = Math.min(minutes, Math.min(minutesLeftToday, Constraints.getMaxMinutes()));
			
			minutes -= shippingMinutes;

			// System.out.println(ldt);
			// System.out.println("Shipping Minutes: " + shippingMinutes + " (a) " + shippingMinutes/60.0 + '\n');

			if(minutes > 0 && shippingMinutes < minutesLeftToday) {
				ldt = ldt.plusMinutes(minutesLeftToday);	// Move to next day after max hours
			} else {
				ldt = ldt.plusMinutes(shippingMinutes);		// Have less hours (automatic transition)
			}
		}
		
		long diffMinutes = (arrive_timezone.getRawOffset() - depart_timezone.getRawOffset())
								/ (1000 * 60);

		return ldt.plusMinutes(diffMinutes);
	}	
}

// Cargo Company (Input)
//---------------------------
// 1. Shipment Date (depart)
// 2. No. of hrs needed
//
// Constraints
//---------------------------
// 1. Max. 12hrs per day
// 2. Holiday - Sat, Sun
// 3. Govt. Holidays - Jan 1, Jan 26, Aug 15
//
// Output
//---------------------------
// Arrival Date and time