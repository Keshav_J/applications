package shipmentCompany;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

import org.junit.Test;

public class JUnitTester {
	private Cargo c; 
	private DateTimeFormatter dtf;
	
	public JUnitTester() {
		this.c = new Cargo();
		this.dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	}
	
	@Test
	public void caseRandom() {
		LocalDateTime departure = LocalDateTime.parse("2020-09-22 16:30", this.dtf);
		TimeZone depart_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		TimeZone arrive_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		
		LocalDateTime expected = LocalDateTime.parse("2020-09-23 02:30", this.dtf);
		int noOfHours = 10;
		
		LocalDateTime arrival = this.c.getArrivalDate(departure, noOfHours, depart_timezone, arrive_timezone);
		
		assertEquals(expected, arrival);
	}
	
	@Test
	public void caseLeapYear() {
		LocalDateTime departure = LocalDateTime.parse("2020-02-28 16:30", this.dtf);
		TimeZone depart_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		TimeZone arrive_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		
		LocalDateTime expected = LocalDateTime.parse("2020-03-04 08:30", this.dtf);
		int noOfHours = 40;
		
		LocalDateTime arrival = this.c.getArrivalDate(departure, noOfHours, depart_timezone, arrive_timezone);
		
		assertEquals(expected, arrival);
	}
	
	@Test
	public void nonLeapYear() {
		LocalDateTime departure = LocalDateTime.parse("2019-02-28 16:30", this.dtf);
		TimeZone depart_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		TimeZone arrive_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		
		LocalDateTime expected = LocalDateTime.parse("2019-03-01 02:30", this.dtf);
		int noOfHours = 10;
		
		LocalDateTime arrival = this.c.getArrivalDate(departure, noOfHours, depart_timezone, arrive_timezone);
		
		assertEquals(expected, arrival);
	}
	
	@Test
	public void caseWeekend() {
		LocalDateTime departure = LocalDateTime.parse("2020-09-18 17:00", this.dtf);
		TimeZone depart_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		TimeZone arrive_timezone = TimeZone.getTimeZone("Asia/Calcutta");

		LocalDateTime expected = LocalDateTime.parse("2020-09-21 03:00", this.dtf);
		int noOfHours = 10;
		
		LocalDateTime arrival = this.c.getArrivalDate(departure, noOfHours, depart_timezone, arrive_timezone);
		
		assertEquals(expected, arrival);
	}

	@Test
	public void caseTimeZone() {
		LocalDateTime departure = LocalDateTime.parse("2020-09-22 16:30", this.dtf);
		TimeZone depart_timezone = TimeZone.getTimeZone("Asia/Calcutta");
		TimeZone arrive_timezone = TimeZone.getTimeZone("Australia/Perth");
		
		LocalDateTime expected = LocalDateTime.parse("2020-09-23 05:00", this.dtf);
		int noOfHours = 10;
		
		LocalDateTime arrival = this.c.getArrivalDate(departure, noOfHours, depart_timezone, arrive_timezone);
		
		assertEquals(expected, arrival);
	}
}