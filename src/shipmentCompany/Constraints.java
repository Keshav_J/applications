package shipmentCompany;

import java.util.List;
import java.util.Arrays;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Constraints {
	static private List<String> holidays, govtHolidays;
	static private int maxMintues;
	
	static  {
		holidays = Holidays.Holidays.getHolidays();
		govtHolidays  = GovtHolidays.GovtHolidays.getGovtHolidays();
		maxMintues = HoursPerDay.HoursPerDay.getHours() * 60;
		
		// System.out.println("Holidays: " + holidays);
		// System.out.println("Dates: " + govtHolidays);
		// System.out.println("Max Hours: " + maxMintues); System.out.println();
	}
	
	static public boolean isHoliday(LocalDateTime ldt) {
		String day = ldt.getDayOfWeek().toString();
				
		for(String d: holidays)
			if(day.equals(d))
				return true;
		
		return false;
	}
	
	static public boolean isGovtHoliday(LocalDateTime ldt) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		for(String day: govtHolidays) {
			day = ldt.getYear() + "-" + day + " 00:00";
			LocalDateTime leave = LocalDateTime.parse(day, dtf);
			
			if(leave.getMonthValue() == ldt.getMonthValue()
					&& leave.getDayOfMonth() == ldt.getDayOfMonth())
				return true;
		}
		
		return false;
	}
	
	static public int getMaxMinutes() {
		return maxMintues;
	}
}

enum Holidays {
	Holidays("SATURDAY", "SUNDAY");
	
	private final List<String> days;

	private Holidays(String ...holidays) {
		this.days = Arrays.asList(holidays);
	}
	
	public List<String> getHolidays() {
		return this.days;
	}
}

enum GovtHolidays {
	GovtHolidays("01-01", "01-26", "08-15");
	
	private final List<String> dates;
	
	private GovtHolidays(String ...values) {
		this.dates = Arrays.asList(values);
	}
	
	public List<String> getGovtHolidays() {
		return this.dates;
	}
}

enum HoursPerDay {
	HoursPerDay(12);
	
	private int hours;

	HoursPerDay(int hrs) {
		this.hours = hrs;
	}
	
	public int getHours() {
		return this.hours;
	}
}